
<?php 
	include("HeaderView.php");
 ?>
 <div id="banner-area">
	<img src="images/banner/banner1.jpg" alt="" />
	<div class="parallax-overlay"></div>
	<!-- Subpage title start -->
	<div class="banner-title-content">
		<div class="text-center">
			<h2>About Us</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb justify-content-center">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item text-white" aria-current="page">About Us</li>
				</ol>
			</nav>
		</div>
	</div><!-- Subpage title end -->
</div><!-- Banner area end -->

<!-- About tab start -->
<section id="about" class="about angle">
	<div class="container">
		<div class="row landing-tab">
			<div class="col-md-3 col-sm-5">
				<div class="nav flex-column nav-pills border-right" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<a class="animated fadeIn nav-link py-4 active d-flex align-items-center" data-toggle="pill" href="#tab_1" role="tab"
						aria-selected="true">
						<i class="fa fa-info-circle mr-4"></i>
						<span class="h4 mb-0 font-weight-bold">Who Are We</span>
					</a>
					<a class="animated fadeIn nav-link py-4 d-flex align-items-center" data-toggle="pill" href="#tab_2" role="tab"
						aria-selected="true">
						<i class="fa fa-briefcase mr-4"></i>
						<span class="h4 mb-0 font-weight-bold">OUR COMPANY</span>
					</a>
					<a class="animated fadeIn nav-link py-4 d-flex align-items-center" data-toggle="pill" href="#tab_3" role="tab"
						aria-selected="true">
						<i class="fa fa-android mr-4"></i>
						<span class="h4 mb-0 font-weight-bold">What We Do</span>
					</a>
					<a class="animated fadeIn nav-link py-4 d-flex align-items-center" data-toggle="pill" href="#tab_4" role="tab"
						aria-selected="true">
						<i class="fa fa-pagelines mr-4"></i>
						<span class="h4 mb-0 font-weight-bold">Modern Design</span>
					</a>
					<a class="animated fadeIn nav-link py-4 d-flex align-items-center" data-toggle="pill" href="#tab_5" role="tab"
						aria-selected="true">
						<i class="fa fa-support mr-4"></i>
						<span class="h4 mb-0 font-weight-bold">Dedicated Support</span>
					</a>
				</div>
			</div>
			<div class="col-md-9 col-sm-7">
				<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane pl-sm-5 fade show active animated fadeInLeft" id="tab_1" role="tabpanel">
						<i class="fa fa-trophy icon-xl text-primary mb-4"></i>
						<h3>We Are Awwared Winning Company</h3>
						<p>	</p>
					</div>
					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_2" role="tabpanel">
						<i class="fa fa-briefcase icon-xl text-primary mb-4"></i>
						<h3>We Have Worldwide Business</h3>
						<p>BitsDecoder is a software solutions and services based company. BitsDecoder is an established Web and Mobile Application Development
							Company delivering  Mobile Application Development and Enterprise Mobility Services of any complexity to clients worldwide.BitsDecoder is a
							growing team of creative, strategic, and professional specialists in web design and
							development industry. We provide end to end solutions from front-end programming to back
							end programming and maintenance. We are young, dynamic, energetic,techno savvy to provide new
							and efficient services for our client powered by excellent minds and creativity. </p>
					</div>
					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_3" role="tabpanel">
						<i class="fa fa-android icon-xl text-primary mb-4"></i>
						<h3>We Build Web and Mobile  Applications</h3>
						<p>We provide continuous web development and Mobile app development solutions that help you arrive at
						a wide target market and meet your aim.</p>
					</div>
					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_4" role="tabpanel">
						<i class="fa fa-pagelines icon-xl text-primary mb-4"></i>
						<h3>Clean and Modern Design</h3>
						<p>Over the year we have lots of experience in our field. In sit amet massa sapien. Vestibulum diam turpis,
							gravida in lobortis id, ornare a eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
							sagittis nulla non elit dignissim suscipit. Duis lorem nulla, eleifend.</p>
					</div>
					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_5" role="tabpanel">
						<i class="fa fa-support icon-xl text-primary mb-4"></i>
						<h3>24/7 Dedicated Support</h3>
						<p>Technology is our passion!. We are available 24/7 hours at any where ,any time for client query.</p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- Container end -->
</section><!-- About end -->

<?php 
	include("FooterView.php");
 ?>
