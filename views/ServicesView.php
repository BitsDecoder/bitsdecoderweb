
<?php 
	include("HeaderView.php");
 ?>

<div id="banner-area">
	<img src="images/banner/banner1.jpg" alt="" />
	<div class="parallax-overlay"></div>
	<!-- Subpage title start -->
	<div class="banner-title-content">
		<div class="text-center">
			<h2>Service</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb justify-content-center">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item text-white" aria-current="page">Service</li>
				</ol>
			</nav>
		</div>
	</div><!-- Subpage title end -->
</div><!-- Banner area end -->

<!-- Main container start -->
<section id="main-container">
	<div class="container">
		<!-- Services -->
		<div class="row">
			<div class="col-md-12 heading">
				<span class="title-icon classic float-left"><i class="fa fa-cogs"></i></span>
				<h2 class="title classic">Our Services</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-3 mb-5 wow fadeInDown" data-wow-delay=".5s">
				<div class="service-content text-center">
					<span class="service-icon icon-pentagon"><i class="fa fa-tachometer"></i></span>
					<h3>Web Design</h3>
					<p>Web design is the visual aesthetics and page layout of a website. It goes hand-in-hand with web development in the creation of a static website or dynamic web application.</p>
				</div>
			</div>
			<!--/ End first service -->

			<div class="col-md-3 col-sm-3 mb-5 wow fadeInDown" data-wow-delay=".8s">
				<div class="service-content text-center">
					<span class="service-icon icon-pentagon"><i class="fa fa-android"></i></span>
					<h3>Android Apps Development</h3>
					<p>We have deep expertise in building native Android apps
					</p>
				</div>
			</div>
			<!--/ End Second service -->

			<div class="col-md-3 col-sm-3 mb-5 wow fadeInDown" data-wow-delay="1.1s">
				<div class="service-content text-center">
					<span class="service-icon icon-pentagon"><i class="fa fa-shopping-cart"></i></span>
					<h3>Xamarin App Development</h3>
					<p>Xamarin has entirely converted the Android and iOS SDK to C# to make it more familiar to the developers. One can easily use the same codebase for both the platforms without the hassle of remembering the syntax of different languages all the time.</p>
				</div>
			</div>
			<!--/ End Third service -->

			<div class="col-md-3 col-sm-3 mb-5 wow fadeInDown" data-wow-delay="1.4s">
				<div class="service-content text-center">
					<span class="service-icon icon-pentagon"><i class="fa fa-lightbulb-o"></i></span>
					<h3>React-native App Development</h3>
					<p>React Native is a framework developed by Facebook for creating native-style apps for iOS & Android under one common language, JavaScript. Initially, Facebook only developed React Native to support iOS. However with its recent support of the Android operating system, the library can now render mobile UIs for both platforms.</p>
				</div>
			</div>

			<div class="col-md-3 col-sm-3 mb-5 wow fadeInDown" data-wow-delay="1.4s">
				<div class="service-content text-center">
					<span class="service-icon icon-pentagon"><i class="fa fa-lightbulb-o"></i></span>
					<h3>Php Development</h3>
					<p>We are developing Project on Core php, CodeIgniter, Laravel , WordPress</p>
				</div>
			</div>
			<!--/ End first service -->

		<!--/ End 4th service -->
		</div><!-- Content 2nd row end -->
		<!-- Services end -->
	</div>
	<!--/ 1st container end -->
	<div class="gap-60"></div>
	<?php 
	include("FooterView.php");
 ?>